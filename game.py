import pydealer
import dealer
import player
import pot
import bestHand
import numpy as np
import csv


print("Welcome to poker! Player 1 is a bot, Player 2 is a person!")
# Add players
players = list()
budget = input('Budget of each player?: ')
# Set ante and blinds
bigBlindVal = int(input('What is the big blind bet size?: '))
smallBlindVal = int(bigBlindVal / 2)

BDE = int(input('Bet-Decision-Factor (BDE) of bot: '))
while BDE < 0 or BDE > 10:
    BDE = int(input('BDE must be an integer between 0-10: '))
players.append(player.Player(budget, 1, 0, BDE)) #chips, bot, ID, BDE
players.append(player.Player(budget, 0, 1, 1)) #chips, bot, ID, BDE

# players.append(player.Player(budget, 1, 1, 1)) #chips, bot, ID, BDE
# players.append(player.Player(budget, 1, 2, 3)) #chips, bot, ID, BDE
# players.append(player.Player(budget, 1, 3, 5))
# players.append(player.Player(budget, 1, 4, 7))

# Add extra players
addPlayers = input('Add another player (y/n)?: ')
while addPlayers == "y":
    bot = int(input('Bot or human (1 for bot)?: '))
    if bot == 1:
        BDE = int(input('Bet-Decision-Factor (BDE) of bot: '))
        while BDE < 0 or BDE > 10:
            BDE = int(input('BDE must be an integer between 0-10: '))
    else:
        BDE = 0

    players.append(player.Player(budget, bot, len(players), BDE))
    addPlayers = input('Add player (y/n)?: ')

with open('gameRecord.csv', mode='a') as gameRecord:
    gameWriter = csv.writer(gameRecord, delimiter = ',')
    gameWriter.writerow(["Bot's chips", "Player's chips"])
    gameWriter.writerow([players[0].chips, players[1].chips])
    #gameWriter.writerow(["Bot 1", "Bot 2", "Bot 3", "Bot 4", "Bot 5"])
    #gameWriter.writerow([players[0].chips, players[1].chips,  players[2].chips,  players[3].chips,  players[4].chips])



# smallBlindVal = int(input('What is the small blind bet size?: '))
deal = dealer.Dealer()
pot = pot.Pot()
playersLeft = len(players)

# Assign blinds to positions
smallBlindPos = 0
bigBlindPos = 1


round = 1
# Iterate for every round
while playersLeft!=0:
    # Reset pot and table
    pot.reset()
    deal.resetTable()

    # Reset hand, deal new hand
    playersLeft = len(players)
    num = 0
    for play in players:
        # Check to see if player is out
        if play.chips <= 0:
            print("Player " + str(play.id + 1) + " sorry, you are out!")
            play.out = 1
            playersLeft = playersLeft-1
        # Check for winner
        if playersLeft == 1:
            break
        # Set up all other players
        play.folded = 0
        play.resetHand()
        hand = deal.dealHand()
        play.setHand(hand)
        num = num+1
    # End Game if nobody left
    if playersLeft == 1:
        for player in players:
            if player.out == False:
                print("Congrats player " + str(player.id + 1) + " you have won!")
        break
    #------------------------------------------------------------
    # BLINDS AND ANTES
    if round != 1:
        ### FINDING THE NEXT SMALL BLIND POSITION
        smallBlindPos += 1

        if smallBlindPos >= len(players):
            smallBlindPos = 0

        while players[smallBlindPos].out == True:
            smallBlindPos += 1

            if smallBlindPos >= len(players):
                smallBlindPos = 0

        ### FINDING THE NEXT BIG BLIND POSITION
        bigBlindPos = smallBlindPos + 1

        if bigBlindPos >= len(players):
            bigBlindPos = 0

        while players[bigBlindPos].out == True:
            bigBlindPos += 1

            if bigBlindPos >= len(players):
                bigBlindPos = 0

    # print('Small blind position is: ' + str(smallBlindPos))
    # print('Big blind position is: ' + str(bigBlindPos))

    ### REMOVING THE BLINDS FROM THE PLAYERS CHIPS AND ADDING THEM TO THE POT

    for player in players:
        if player.id == smallBlindPos:
            if player.chips <= smallBlindVal:
                pot.add(player.chips)
                smallBlindActual = player.chips
                player.changeChips(-1 * player.chips)
                player.allIn == True
            else:
                pot.add(smallBlindVal)
                smallBlindActual = smallBlindVal
                player.changeChips(-1 * smallBlindVal)


        if player.id == bigBlindPos:
            if player.chips <= bigBlindVal:
                pot.add(player.chips)
                bigBlindActual = player.chips
                player.changeChips(-1 * player.chips)
                player.allIn == True
            else:
                pot.add(bigBlindVal)
                bigBlindActual = bigBlindVal
                player.changeChips(-1 * bigBlindVal)



        # smallBlindPos = smallBlindPos+1
        # while 1==1:
        #     if smallBlindPos>=playersLeft:
        #         smallBlindPos = 0
        #         break
        #     if players[smallBlindPos].out == 1:
        #         smallBlindPos = smallBlindPos+1
        #         continue
        #     if players[smallBlindPos].out == 0:
        #         break
        # bigBlindPos = smallBlindPos+1
        # while 1 == 1:
        #     if bigBlindPos >= playersLeft:
        #         bigBlindPos = 0
        #         break
        #     if players[bigBlindPos].out == 1:
        #         bigBlindPos = bigBlindPos + 1
        #         continue
        #     if players[bigBlindPos].out == 0:
        #         break
    # blindNum = 0
    # for play in players:
    #     potInput = 0
    #     # Collect blinds for all of those who havent lost
    #     if blindNum == smallBlindPos:
    #         potInput = smallBlindVal
    #     elif blindNum == bigBlindPos:
    #         potInput = bigBlindVal
    #     if play.out == 0:
    #         if play.chips < potInput:
    #             play.changeChips(potInput - play.chips)
    #             pot.add(potInput - play.chips)
    #         else:
    #             play.changeChips(-1 * potInput)
    #             pot.add(potInput)
    #     blindNum = blindNum+1

    # ------------------------------------------------------------
    # RUN ROUND
    # Get new cards for table
    playersLeftRound = len(players)
    for i in range(len(players)):
        if players[i].out:
            playersLeftRound -= 1

    for cardFlip in range(0,3):
        if cardFlip == 0:
            deal.dealFlop()
        elif cardFlip == 1:
            deal.dealTurn()
        elif cardFlip == 2:
            deal.dealRiver()
        print("------------------------------------------------------------")
        print("TABLE:")
        print(deal.table)
        print("Pot size: ", pot.getSize())
        print("------------------------------------------------------------")
        # Show hand if real player, get choices of players
        highestBet = 0

        if cardFlip == 0:
            highestBet = bigBlindVal

        # curBets[i] is the amount players[i] has added to the pot in this round
        # curBets[i] is the amount players[i] has added to the pot in this round
        curBets = list()
        for i in range(len(players)):
            if cardFlip == 0:
                if i == bigBlindPos:
                    if player.chips <= bigBlindVal:
                        chipsChange = player.chips
                    else:
                        chipsChange = bigBlindVal
                    curBets.append(chipsChange)
                    continue
                if i == smallBlindPos:
                    if player.chips <= smallBlindVal:
                        chipsChange = player.chips
                    else:
                        chipsChange = smallBlindVal
                    curBets.append(chipsChange)
                    continue
            curBets.append(0)

        # alreadyBet[i] is true if players[i] has already been asked what they'd like to do in a given round
        alreadyBet = list()
        alreadyRaised = list()
        for i in range(len(players)):
            alreadyBet.append(False)
            alreadyRaised.append(False)

        stay = True

        while stay:
            for i in range(0,len(players)):
                # break if only one player is left
                if playersLeftRound <= 1:
                    break

                # everyone at the table checked and we need to begin the next round
                if curBets[i] == highestBet and alreadyBet[i] == True:
                    break

                # players[i] cannot make any more decisions
                if players[i].folded == 1 or players[i].out == 1 or players[i].allIn == 1:
                    alreadyBet[i] = True
                    continue


                if not players[i].bot:
                    hand = players[i].hand
                    x = i+1
                    print("------------------------------------------------------------")
                    print("Player " + str(x) + " here is your hand:")
                    print(hand)
                    print("Chips: " + str(players[i].chips))


                othersChips = list()
                for player in players:
                    tempTuple = (not (player.folded or player.out), player.chips)
                    othersChips.append(tempTuple)

                choice, bet = players[i].getChoice(deal.table, curBets[i], highestBet, highestBet == curBets[i], playersLeftRound, pot.size, bigBlindVal, othersChips, alreadyRaised[i])#, OTHERSCHIPS = []) #IMPLEMENT OTHERS CHIPS
                pot.add(bet)
                curBets[i] = curBets[i] + bet
                if curBets[i] > highestBet:
                    highestBet = curBets[i]
                alreadyBet[i] = True

                if choice == 'fold':
                    playersLeftRound -= 1

                if choice == 'raise':
                    alreadyRaised[i] = True


                if players[i].bot:
                    if choice == 'fold':
                        print("Player " + str(i + 1) + ' folded.')
                    if choice == 'check':
                        print("Player " + str(i + 1) + ' checked.')
                    if choice == 'call':
                        print("Player " + str(i + 1) + ' called.')
                    if choice == 'raise':
                        print("Player " + str(i + 1) + ' bet ' + str(bet) + ' more.')


                # Implement changes to game
                # potAddition = players[i].processChoice(choice, deal.table, highestBet,bet)
                # curBets.append(potAddition)
                # if potAddition > highestBet:
                #     highestBet = potAddition
                # pot.add(potAddition)
            stay = False
            for i in range(len(players)):
                if not ((curBets[i] == highestBet and alreadyBet[i] == True) or players[i].folded or players[i].allIn):
                    stay = True

            if playersLeftRound <= 1:
                stay = False

        # for i in range(len(curBets)):
        #     pot.add(curBets[i])
        #
        # if cardFlip == 0:
        #     pot.add(bigBlindActual * -1)
        #     pot.add(smallBlindActual * -1)


        # for i in range(0, len(players)):
        #     if players[i].folded == 1 or players[i].out == 1 or players[i].allIn == 1:
        #         continue
        #     if curBets[i] < highestBet: # Player has not matched highest bet
        #         if players[i].bot:
        #             # require bot to bet to stay in
        #             print("Bot must decide to call or not. Still need to implement.")
        #
        #         else:
        #             # require human to bet to stay in
        #             diff = highestBet - curBets[i]
        #             print("Player", i+1, "you must bet", diff, "chips to call. Would you like to call or fold?")
        #             ans = input("call/fold: ") # Verify valid input here
        #             if ans == "call":
        #                 players[i].processChoice("call", deal.table, diff)
        #                 print("I just called.")
        #             elif ans == "fold":
        #                 players[i].folded = 1
        #                 print("Player", i, "just folded.")
        # ------------------------------------------------------------
    # COMPARE CARDS AT END
    print("All right now who wins?")
    utility = list()
    num = 0
    for i in range(0, len(players)):
        if players[i].folded == 1 or players[i].out == True:
            utility.append(-1)
        else:
            [utilityOfHand, matchType, highCards] = bestHand.findBestHand(players[i].hand, deal.table)
            utility.append(utilityOfHand)
            print("Player " + str(i+1) + " you had:")
            print(players[i].hand)
            print("So you have a " + matchType)
    winner = np.argmax(utility)
    print("The winner is player " + str(winner+1) + "! " +str(pot.getSize()) + " is added to your chips")
    print("------------------------------------------------------------")
    print("-------------------------NEW HAND---------------------------")
    print("------------------------------------------------------------")
    players[winner].changeChips(pot.getSize())
    for play in players:
        if play.chips != 0:
            play.allIn = 0

    with open('gameRecord.csv', mode='a') as gameRecord:
        gameWriter = csv.writer(gameRecord, delimiter = ',')
        gameWriter.writerow([players[0].chips, players[1].chips])
        #gameWriter.writerow([players[0].chips, players[1].chips,  players[2].chips,  players[3].chips,  players[4].chips])


    round = round+1
