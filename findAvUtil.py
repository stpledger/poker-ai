import pydealer
from pydealer.const import POKER_RANKS
import time
import bestHandcopy

trials = 108192
timetrials = 10
u =0
u1 = 0
u2 = 0

start = time.time()
for t in range(timetrials):
	for i in range(trials):
		deckAB = pydealer.Deck()
		deckAB.shuffle()

		handA = deckAB.deal(2)
		handB = deckAB.deal(2)
		tableAB = deckAB.deal(5)

		[utility1, bestSet1, top51]  = bestHandcopy.findBestHand(handA, tableAB)
		[utility2, bestSet2, top52]  = bestHandcopy.findBestHand(handB, tableAB)
		u1 = u1 + utility1
		u2 = u2 + utility2

		#deck = pydealer.Deck()
		#deck.shuffle()

		#hand = deck.deal(2)
		#table = deck.deal(5)
		#[util, bs, top5] = bestHand.findBestHand(hand, table)
		#u = u + util

u = u / trials/timetrials
u1 = u1 / trials/timetrials
u2 = u2 / trials/timetrials
end = time.time()
print('Time: ', (end - start)/timetrials)
print('Average A: ', u1)
print('Average B: ', u2)
print('Average Solo: ', u)