import pydealer
import bestHand
import sim
import random

class Player:

    def __init__(self, chips, bot, id, BDE):
        self.chips = int(chips)
        self.hand = pydealer.Stack()
        self.bot = bot # if bot is true, the player will be controlled by the AI. If false, it will prompt the terminal for the choice.
        self.folded = False
        self.out = False
        self.allIn = False
        self.silence = True
        self.id = id
        self.BDE = BDE

    def setHand(self, hand):
        self.hand += hand

    def resetHand(self):
        self.folded = False
        self.hand = pydealer.Stack()

    def changeChips(self, amount):
        self.chips = self.chips+amount
        if self.chips > 0:
            self.allIn = False

    # def shouldRaise(self, curWinProb):
    #     return curWinProb > .2

    def shouldBet(self, curWinProb, playersLeft, bigBlindVal, potSize, othersChips, callAmount): #only called in check/bet situation

        betSensisitivy = .3 #OPTIMIZE - used to determine when to raise. between (0-10)

        trueRand = (random.random() - .5)/5 #random num between -.1 to .1

        bdeRand = random.random()
        if bdeRand > (self.BDE / 50): #if random number is higher than 5-50% (based on BDE) then will increase raise amount further
            bdeAmount = .15 #THIS NUMBER IS RANDOM, FIND WAY TO OPTIMIZE
        else:
            bdeAmount = 0

        adjWinProb = curWinProb*playersLeft + bdeAmount + trueRand

        betBool = adjWinProb > 1/betSensisitivy

        if betBool:
            return betBool, self.calcRaiseAmount(curWinProb, playersLeft, bigBlindVal, potSize, othersChips, callAmount) #bet if favorable hand, .75 is a temporary BDE example
        else:
            return betBool, 0

    def shouldRaise(self, curWinProb, playersLeft, bigBlindVal, potSize, othersChips, callAmount):
        payoff = callAmount / potSize
        raiseSensitivity = .9 #.3 #OPTIMIZE - used to determine when to raise. Between (1-5ish)

        trueRand = (random.random() - .5)/5 #random num between -.1 to .1

        bdeRand = random.random()
        if bdeRand > (self.BDE / 50): #if random number is higher than 5-50% (based on BDE) then will increase raise amount further
            bdeAmount = .1 #THIS NUMBER IS RANDOM, FIND WAY TO OPTIMIZE
        else:
            bdeAmount = 0

        adjWinProb = curWinProb / raiseSensitivity + bdeAmount + trueRand

        raiseBool = (adjWinProb >= payoff)

        if raiseBool:
            return raiseBool, self.calcRaiseAmount(curWinProb, playersLeft, bigBlindVal, potSize, othersChips, callAmount)
        else:
            return raiseBool, 0

    def shouldCall(self, curWinProb, playersLeft, bigBlindVal, potSize, othersChips, callAmount):
        payoff = callAmount / (potSize + callAmount)
        if potSize < bigBlindVal * 2: #checking for calling pre-flop, since the pot will be the same size as the big blind. If not checked, the bot will almost always fold pre-flop
            return curWinProb > .15, (curWinProb > .15) * callAmount #arbitrarily picking bot should stay in to see flop if win prob greater than 15% OPTIMIZE!!
        callSensitivity = .3  #OPTIMIZE (between 0-5ish)
        bde = (self.BDE - 5)/50 #makes BDE between -.1 to .1
        bde = bde * callSensitivity #adjusts how much BDE should affect. Ex callSensitivity of 1 ranges bde between -.1 to 1, where .5 ranges bde between -.05-.05, and 2 ranges -.2-.2

        trueRand = (random.random() - .5)/5 #random num between -.1 to .1

        callingBet = callAmount

        if curWinProb >= payoff:
            if callAmount >= self.chips:
                callingBet = self.chips
            else:
                callingBet = callAmount
        else:
            callingBet = 0



        return (curWinProb + bde + trueRand >= payoff), callingBet #returns if it should call (including bde agressiveness), and how much if so

    def calcRaiseAmount(self, curWinProb, playersLeft, bigBlindVal, potSize, othersChips, callAmount):
        trueRand = (random.random() - .5) #random num between -.5 to .5
        trueRand = int(trueRand * bigBlindVal) #random amount that is between 50% lower and 50% higher than big blind.
        #implement folding bet (try to get players to fold)
        if curWinProb*playersLeft < 1: #if hand is relatively weak compared to table
            betAmount = bigBlindVal * 3 + trueRand #bet amount will be between 2.5-3.5 big blind
        #implement value bet (try to get players to call, lower bet encourages them staying in)
        else:
            betAmount = bigBlindVal * 2 + trueRand #bet amount will be between 1.5-2.5 big blind

        if betAmount < callAmount:
            betAmount = callAmount + bigBlindVal//3

        #implement BDE effect - only increases bet amount. BDE takes care of bluffing in terms of amount bluffed
        bdeRand = random.random()
        if bdeRand > (self.BDE / 50): #if random number is higher than 5-50% (based on BDE) then will increase raise amount further
            bdeAmount = bigBlindVal//2 #THIS NUMBER IS RANDOM, FIND WAY TO OPTIMIZE
        else:
            bdeAmount = 0

        betAmount = betAmount + bdeAmount #adds bde chips to bet amount

        if betAmount > self.chips:
            betAmount = self.chips
        if betAmount > callAmount:
            return betAmount
        else:
            return 0

    # def shouldFold(self, curWinProb, mustRaise = 0):
    #     return curWinProb <= .1

    def getChoice(self, table, myCurBet, highestBet, canCheck, playersLeft, potSize, bigBlindVal, othersChips, alreadyRaised):#, othersChips):
        #This is where our AI algorithm is going make the decision on the best move
        myBet = 0
        if canCheck: #CHECK/BET SITUATION
            if not self.bot:
                decision = input('What would you like to do (check/bet)? ')
                while decision != 'check' and decision != 'bet' :
                    decision = input('What would you like to do (please type: check/bet)? ')
                if decision == 'bet':
                    myBet = int(input('How much would you like to bet? '))
                    while myBet > self.chips:
                        myBet = int(input('You only have ' + str(self.chips) + ' chips. How much would you like to bet? '))

                self.changeChips(myBet * -1)
                if self.chips == 0:
                    self.allIn = True
                    print("Going all in")
                return decision, myBet
            else: #CHECK/BET SITUATION
                # Run AI logic
                print("\nPlayer " + str(self.id + 1) + ":")
                print('BOT has ' +  str(self.chips) + ' chips in a check/bet situation')
                curSim = sim.Sim(self.hand, table, playersLeft)  # NEED TO ACCOUNT FOR NUMBER OF PLAYERS HERE
                curWinProb = curSim.run(1000)
                if not self.silence:
                    print("My probability of winning: ", curWinProb)
                betBool, myBet = self.shouldBet(curWinProb, playersLeft, bigBlindVal, potSize, othersChips, highestBet - myCurBet)
                if betBool:
                    if not self.silence:
                        print("I should bet.")

                    self.changeChips(myBet * -1)
                    if self.chips == 0:
                        self.allIn = True

                        print("Going all in")
                    return "raise", myBet
                else:
                    if not self.silence:
                        print("I shouldn't bet.")
                    myBet = 0
                    self.changeChips(myBet * -1)
                    if self.chips == 0:
                        self.allIn = True

                        print("Going all in")
                    return "check", myBet

                    # Bluff is going to be handled in shouldBet() now !!!!!!!!!!!!!!!!!!!!!!!!
                    # randNum = random.random()
                    # if randNum < 0.2:
                    #     if not self.silence:
                    #         print("I shouldn't bet, but I'm going to.")
                    #     myBet = self.calcRaiseAmount(curWinProb, 0, playersLeft, bigBlindVal)
                    #     self.changeChips(myBet * -1)
                    #     if self.chips == 0:
                    #         self.allIn = True
                    #         self.out = True
                    #         print("Going all in")
                    #     return "raise", myBet
                    # else:
                    #     if not self.silence:
                    #         print("I shouldn't bet.")
                    #     self.changeChips(myBet * -1)
                    #     if self.chips == 0:
                    #         self.allIn = True
                    #         self.out = True
                    #         print("Going all in")
                    #     return "check", myBet

        else: #CALL/RAISE/FOLD SITUATION
            if not self.bot:
                print('The highest bet is ' + str(highestBet) + " and you've bet " + str(myCurBet) + ' so far.')
                decision = input('What would you like to do (call/raise/fold)? ')
                while decision != 'call' and decision != 'raise' and decision != 'fold':
                    decision = input('What would you like to do (please type: call/raise/fold)? ')
                if decision == 'call':
                    if highestBet - myCurBet >= self.chips:
                        myBet = self.chips
                    else:
                        myBet = highestBet - myCurBet
                if decision == 'raise':
                    myBet = int(input('How much would you like to raise over the highest bet? ')) + (highestBet - myCurBet)
                    while myBet > self.chips:
                        myBet = int(input('You only have ' + str(self.chips) + ' chips. How much would you like to raise? '))
                if decision == 'fold':
                    self.folded = True
                    myBet = 0
                self.changeChips(myBet * -1)
                if self.chips == 0:
                    self.allIn = True

                    print("Going all in")
                return decision, myBet
            else: #CALL/RAISE/FOLD SITUATION
                # THIS IS WHERE ALL OF THE HARD AI LOGIC WILL COME INTO PLAY
                print("\nPlayer " + str(self.id + 1) + ":")
                print('BOT has ' + str(self.chips) + ' chips in a call/raise/fold situation')
                curSim = sim.Sim(self.hand, table, playersLeft)  # NEED TO ACCOUNT FOR NUMBER OF PLAYERS HERE
                curWinProb = curSim.run(1000)
                if not self.silence:
                    print("My probability of winning: ", curWinProb)
                if not alreadyRaised:
                    raiseBool, myBet = self.shouldRaise(curWinProb, playersLeft, bigBlindVal, potSize, othersChips, highestBet - myCurBet) #Calculates if it should raise. If not: decides between call and fold. #should be: current win prob, call amount, pot size
                    if raiseBool:
                        if not self.silence:
                            print("I should raise.")

                        self.changeChips(myBet * -1)
                        if self.chips == 0:
                            self.allIn = True

                            print("Going all in")
                        if myBet != 0:
                            return "raise", myBet

                callBool, myBet = self.shouldCall(curWinProb, playersLeft, bigBlindVal, potSize, othersChips, highestBet - myCurBet)
                if callBool:
                    if not self.silence:
                        print("I should call")
                    if highestBet - myCurBet >= self.chips:
                        myBet = self.chips
                    else:
                        myBet = highestBet - myCurBet
                    self.changeChips(myBet * -1)
                    if self.chips == 0:
                        self.allIn = True

                        print("Going all in")
                    return "call", myBet

                else:
                    if not self.silence:
                        print("I fold.")
                    myBet = 0
                    self.folded = True
                    return "fold", myBet





        # if not self.bot:
        #     myBet = 0
        #     decision = input('What would you like to do (check/call/raise/fold)? ')
        #     while decision != 'check' and decision != 'call' and decision != 'raise' and decision != 'fold':
        #         decision = input('What would you like to do (please type: check/call/raise/fold)? ')
        #     if decision == 'raise':
        #         myBet = int(input('How much would you like to raise? '))
        #         while myBet > self.chips:
        #             myBet = int(input('You only have ' + str(self.chips) + ' chips. How much would you like to raise? '))
        #         changeChips(-1 * myBet)
        #     return decision, myBet
        #
        # if self.bot:
        #     # Run AI logic
        #     print("BOT:")
        #     curSim = sim.Sim(self.hand, table, 1)  # NEED TO ACCOUNT FOR NUMBER OF PLAYERS HERE
        #     curWinProb = curSim.run(1000)
        #     print("My probability of winning: ", curWinProb)
        #     if curWinProb >= 0.5:  # Arbitrary number here. We will optimize later.
        #         print("I should bet.")
        #         # Calculate amount to bet here
        #         myBet = self.chips // 2
        #         return "raise", myBet
        #     else:
        #         print("I shouldn't bet.")
        #         return "check", 0

    def processChoice(self, choice, table, highestBet, bet=0):
        potAdd = 0
        if choice == "raise":
            if not self.bot: # Ask player for bet amount
                betAmount = int(input("How much would you like to raise?: "))
            else: # Calculate what the AI should bet
                betAmount = bet
            print("I just raised by", betAmount)
            potAdd = potAdd + betAmount
            lostChips = -1*betAmount
            self.changeChips(lostChips)
            if self.chips < 0:
                print("You don't have enough chips to bet that!")
                self.changeChips(betAmount)
                self.processChoice(choice,table,highestBet)
            if betAmount < highestBet:
                print("You need to match the highest bet!")
                self.changeChips(betAmount)
                allIn = input("Would you like to go all in (y/n)?: ")
                if allIn == "y":
                    print("All in!")
                    self.allIn = 1
                    betAmount = highestBet - self.chips
                    print("I just raised by", betAmount)
                    potAdd = potAdd + betAmount
                    lostChips = -1 * betAmount
                    self.changeChips(lostChips)
                else:
                    choice = self.getChoice(0, table)
                    self.processChoice(choice,table, highestBet)
            elif self.chips == 0:
                print("All in!")
                self.allIn = 1
        if choice == "call":
            betAmount = highestBet
            potAdd = potAdd + betAmount
            lostChips = -1*betAmount
            self.changeChips(lostChips)
            if self.chips < 0:
                print("You don't have enough chips to bet that!")
                self.changeChips(betAmount)
                allIn = input("Would you like to go all in (y/n)?: ")
                if allIn == "y":
                    print("All in!")
                    self.allIn = 1
                    betAmount = highestBet - self.chips
                    print("I just raised by", betAmount)
                    potAdd = potAdd + betAmount
                    lostChips = -1 * betAmount
                    self.changeChips(lostChips)
                else:
                    choice = self.getChoice(0, table)
                    self.processChoice(choice, table, highestBet)
        if choice == "fold":
            self.folded = 1
        if choice == "check":
            if highestBet != 0:
                print("You need to match the highest bet!")
                choice = self.getChoice(0, table)
                self.processChoice(choice,table, highestBet)
        return potAdd
