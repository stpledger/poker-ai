import pydealer

class Dealer:

    def __init__(self):
        self.deck = pydealer.Deck()
        self.deck.shuffle()
        self.table = pydealer.Stack()

    def dealFlop(self):
        self.deck.deal(1)
        self.flop = self.deck.deal(3)
        self.table += self.flop

    def dealTurn(self):
        self.deck.deal(1)
        self.turn = self.deck.deal(1)
        self.table += self.turn

    def dealRiver(self):
        self.deck.deal(1)
        self.river = self.deck.deal(1)
        self.table += self.river

    def dealHand(self):
        newHand  = self.deck.deal(2)
        return newHand

    def resetTable(self):
        self.deck = pydealer.Deck()
        self.deck.shuffle()
        self.table = pydealer.Stack()

    def getTable(self):
        return self.table
