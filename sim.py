import pydealer
import player
import bestHand
import numpy as np
import bestHand

class Sim:

    def __init__(self, myHand, table, numOpponents):
        self.myHand = myHand
        self.numOpponents = numOpponents
        self.table = table

        # configure deck to have every card we don't know the state of in the game (could be in someone's hand or the discard pile, but it doesn't matter to us because for all we know it could still be in the deck)
        self.deck = pydealer.Deck()
        self.cards = list()
        for i in range(self.table.size):
            self.cards.append(str(self.table[i]))
        for i in range(self.myHand.size):
            self.cards.append(str(self.myHand[i]))
        self.deck.get_list(self.cards)


    def run(self, times):
        self.won = 0
        self.tie = 0
        self.lost = 0
        for i in range(times):

            # create copies of the deck and table
            self.tempDeck = pydealer.Deck()
            self.tempDeck.set_cards(self.deck)
            self.tempDeck.shuffle()
            self.tempTable = pydealer.Stack()
            self.tempTable.set_cards(self.table)

            # print('My hand is: ')
            # print(self.myHand)
            # print()


            # create players for the simulation and deal them cards
            self.tempPlayers = list()
            for k in range(self.numOpponents):
                self.tempPlayers.append(player.Player(0, 0, k, 5))
                self.tempPlayers[k].setHand(self.tempDeck.deal(2))

            # print('My opponents hand is: ')
            # print(self.tempPlayers[0].hand)
            # print()

            # deal the remaining cards to the table
            while self.tempTable.size < 5:
                self.tempTable += self.tempDeck.deal(1)

            # print('The table is: ')
            # print(self.tempTable)
            # print()

            # figure out if you won or not
            [self.myUtility, bestSet, highCard]  = bestHand.findBestHand(self.myHand, self.tempTable)

            self.utility = list()
            for k in range(self.numOpponents):
                [self.tempUtility, b, c] = bestHand.findBestHand(self.tempPlayers[k].hand, self.tempTable)
                self.utility.append(self.tempUtility)

            self.maxOpp = np.max(self.utility)

            if self.myUtility > self.maxOpp:
                # print('Win')
                self.won += 1
            elif self.myUtility == self.maxOpp:
                # print('Tie')
                self.tie += 1
            else:
                # print('Lost')
                self.lost += 1
            # print('----------------------------------------------------')

        # print('Won ' + str(self.won) + ' times')
        # print('Tied ' + str(self.tie) + ' times')
        # print('Lost ' + str(self.lost) + ' times')
        return (self.won + self.tie / 2) / times
