import pydealer
#contains function findBestHand

#arguments: 
#hand: stack that contains player's hand
#table: stack that contains cards on the table

#returns:
#utility: int that represents utility of hand
#set: string that contains name of highest set
#top5: stack that contains top 5 cards

#[utility, handset, top5] = findBestHand(hand, table)

#------------------SORTING DICTIONARIES------------------------------
# Define a new rank dict, ``new_ranks``, with ranks for card faces only.
DEFAULT_RANKS = {
        "Ace": 13,
        "King": 12,
        "Queen": 11,
        "Jack": 10,
        "10": 9,
        "9": 8,
        "8": 7,
        "7": 6,
        "6": 5,
        "5": 4,
        "4": 3,
        "3": 2,
        "2": 1
    }

reverse_ranks = {
    "values": {
        "Ace": 1,
        "King": 2,
        "Queen": 3,
        "Jack": 4,
        "10": 5,
        "9": 6,
        "8": 7,
        "7": 8,
        "6": 9,
        "5": 10,
        "4": 11,
        "3": 12,
        "2": 13
    }
}
# Define a new rank dict, with ranks for card suits only.
suit_ranks= {
	"suits": {
        "Spades": 4,
        "Hearts": 3,
        "Clubs": 2,
        "Diamonds": 1
    }}


def findBestHand(hand, table):

	currentHand = hand + table
	#print(hand)
	#print(table)
	currentHand.sort()

	flushSuit = ''
	isFlush = 0

	#calculates flush
	suitSortedHand = currentHand
	suitSortedHand.sort(suit_ranks)

	flushCards = pydealer.Stack()
	tempFlushCards = pydealer.Stack()
	flushNum = 0

	for curCardIndex in range(len(suitSortedHand)):
		if len(currentHand)-1 >= curCardIndex + 4:

			if suitSortedHand[curCardIndex].suit == suitSortedHand[curCardIndex+1].suit:
				
				

				if suitSortedHand[curCardIndex].suit == suitSortedHand[curCardIndex+2].suit:
					

					if suitSortedHand[curCardIndex].suit == suitSortedHand[curCardIndex+3].suit:
						

						if suitSortedHand[curCardIndex].suit == suitSortedHand[curCardIndex+4].suit:
							isFlush = 1
							#if first flush, fill out temp flush cards	
							if flushNum > 0: 
								if DEFAULT_RANKS[flushCards[4].value] < DEFAULT_RANKS[suitSortedHand[curCardIndex+4].value]: #greatest card greater than prev greatest card
									flushSuit = suitSortedHand[curCardIndex].suit
										
									
									flushCards.empty()
									flushCards.add(suitSortedHand[curCardIndex])
									flushCards.add(suitSortedHand[curCardIndex+1])
									flushCards.add(suitSortedHand[curCardIndex+2])
									flushCards.add(suitSortedHand[curCardIndex+3])
									flushCards.add(suitSortedHand[curCardIndex+4])

							else: #if first time flush
								flushNum = flushNum + 1
								flushSuit = suitSortedHand[curCardIndex].suit
								flushCards.empty()
								flushCards.add(suitSortedHand[curCardIndex])
								flushCards.add(suitSortedHand[curCardIndex+1])
								flushCards.add(suitSortedHand[curCardIndex+2])
								flushCards.add(suitSortedHand[curCardIndex+3])
								flushCards.add(suitSortedHand[curCardIndex+4])
						else:
							tempFlushCards.empty()
					else:
						tempFlushCards.empty()
				else:
					tempFlushCards.empty()
			else:
				tempFlushCards.empty()
				
					
						

	repeat = 0
	isStraight = 0
	straightHighVal = ''
	straightCards = pydealer.Stack()
	tempStraightCards = pydealer.Stack()
	sc = 1 #straight count - counts amount of cards in a row for a potential straight
	#calculates straight
	currentHand.sort(reverse_ranks)

	for cci in range(len(currentHand)):
		if len(currentHand)-1 >= cci + 1:
			if DEFAULT_RANKS[currentHand[cci+1].value] - DEFAULT_RANKS[currentHand[cci].value] == -1: #if there are 2 cards in a row
				if sc == 4: #if fifth in a row
					sc = 5
					tempStraightCards.add(currentHand[cci+1])
					isStraight = 1
					for card in tempStraightCards:
						straightCards.add(card)

				if sc == 3: #if fourth in a row
					sc = 4
					tempStraightCards.add(currentHand[cci+1])
				if sc == 2: #if third in a row
					sc = 3
					tempStraightCards.add(currentHand[cci+1])
				if sc == 1: #if first 2 in a row found
					sc = 2
					tempStraightCards.add(currentHand[cci])
					tempStraightCards.add(currentHand[cci+1])
				
				
				
			elif DEFAULT_RANKS[currentHand[cci+1].value] - DEFAULT_RANKS[currentHand[cci].value] != 0:
				sc = 1
				tempStraightCards.empty()
	#----------------------------------------------------------------------------------------------------------------------------------------------------------------------

	currentHand.sort(reverse_ranks)
	is4kind = 0
	fourKindVal = ''
	fourKindCards = pydealer.Stack()
	#calculates 4 of a kind
	for curCardIndex in range(len(currentHand)):

		if len(currentHand)-1 >= curCardIndex + 3:
			if currentHand[curCardIndex].value == currentHand[curCardIndex+1].value:
				if currentHand[curCardIndex].value == currentHand[curCardIndex+2].value:
					if currentHand[curCardIndex].value == currentHand[curCardIndex+3].value:
						is4kind = 1
						fourKindVal = currentHand[curCardIndex].value
						fourKindCards.add(currentHand[curCardIndex])
						fourKindCards.add(currentHand[curCardIndex+1])
						fourKindCards.add(currentHand[curCardIndex+2])
						fourKindCards.add(currentHand[curCardIndex+3])

						for card in currentHand:
							#add card as fifth unless its already there
							if card not in fourKindCards:
								fourKindCards.add(card)
								break
							



	#calculate three of a kind
	is3kind = 0
	threeKindVal = ''
	tempVal = ''
	isTemp = 0
	threeKindCards = pydealer.Stack()
	tempFullHouseCards3 = pydealer.Stack()
	currentHand.sort()

	for curCardIndex in range(len(currentHand)):
		for nextCardIndex in range(curCardIndex+1, len(currentHand)): 
			if currentHand[curCardIndex].value == currentHand[nextCardIndex].value and isTemp == 1 and tempVal == currentHand[curCardIndex].value:

				threeKindVal = currentHand[curCardIndex].value
				is3kind = 1

				#add to stacks. make one for a full house (dont add any more cards) and make one for just a 3 of a kind (finds next two best cards)
				tempFullHouseCards3.empty()
				tempFullHouseCards3.add(currentHand[nextCardIndex])
				tempFullHouseCards3.add(currentHand[curCardIndex])
				tempFullHouseCards3.add(currentHand[curCardIndex-1])

			if currentHand[curCardIndex].value == currentHand[nextCardIndex].value:
				tempVal = currentHand[curCardIndex].value
				isTemp = 1

	pairCards = pydealer.Stack()
	pairVal = ''
	isPair = 0
	#calculate pairs if not already in the 3 of a kind or 4 of a kind
	for curCardIndex in range(len(currentHand)):
		for nextCardIndex in range(curCardIndex+1, len(currentHand)):
			if currentHand[curCardIndex].value == currentHand[nextCardIndex].value and currentHand[curCardIndex].value != threeKindVal:
				pairVal = currentHand[curCardIndex].value
				isPair = 1
				pairCards.empty()
				pairCards.add(currentHand[curCardIndex])
				pairCards.add(currentHand[nextCardIndex])


	
	secPairVal = ''
	is2Pair = 0	

	revSortedHand = currentHand
	revSortedHand.sort(reverse_ranks)
	pairCards2 = pydealer.Stack()

	#calculate 2nd pair if not already in the 3 of a kind or prev pair	
	for curCardIndex in range(len(revSortedHand)):
		for nextCardIndex in range(curCardIndex+1, len(revSortedHand)):		
			if is2Pair == 0 and revSortedHand[curCardIndex].value == revSortedHand[nextCardIndex].value and revSortedHand[curCardIndex].value != threeKindVal and revSortedHand[curCardIndex].value != pairVal:
				secPairVal = revSortedHand[curCardIndex].value
				is2Pair = 1
				pairCards2.empty()
				pairCards2.add(currentHand[curCardIndex])
				pairCards2.add(currentHand[nextCardIndex])


#---------------------------------RETURNS/UTILITY----------------------------------------
	#calculate if 
	# if straight flush (9)
	straightCards.sort(reverse_ranks)
	flushCards.sort(reverse_ranks)

	utility = 0

	if isStraight == 1 and isFlush == 1: #if have both a straight and a flush
		if flushCards == straightCards: #if deck instances contain same cards
			string = 'Straight Flush'
			utility = utility + 9e10

			util0 = DEFAULT_RANKS[straightCards[0].value] * 10e8/2
			util1 = DEFAULT_RANKS[straightCards[1].value] * 10e6/2
			util2 = DEFAULT_RANKS[straightCards[2].value] * 10e4/2
			util3 = DEFAULT_RANKS[straightCards[3].value] * 10e2/2
			util4 = DEFAULT_RANKS[straightCards[4].value] * 10e0/2
			utility = utility + util0 + util1 + util2 + util3 + util4

			return utility, string, straightCards

	# if four of a kind (8)
	if is4kind == 1:
		string = 'Four of a kind of ' + fourKindVal + 's'
		utility = utility + 8e10

		fourKindCards.sort(reverse_ranks)

		util0 = DEFAULT_RANKS[fourKindCards[0].value] * 10e8/2
		util1 = DEFAULT_RANKS[fourKindCards[1].value] * 10e6/2
		util2 = DEFAULT_RANKS[fourKindCards[2].value] * 10e4/2
		util3 = DEFAULT_RANKS[fourKindCards[3].value] * 10e2/2
		util4 = DEFAULT_RANKS[fourKindCards[4].value] * 10e0/2

		utility = utility + util0 + util1 + util2 + util3 + util4

		return utility, string, fourKindCards

	# if full house (7)
	if is3kind == 1 and isPair == 1 and pairVal != threeKindVal:
		string = 'Full House of 3 ' + threeKindVal +'s and 2 ' + pairVal + 's'
		fullHouseCards = pydealer.Stack()
		for card in tempFullHouseCards3:
			fullHouseCards.add(card)
		for card in pairCards:
			fullHouseCards.add(card)
		utility = utility + 7e10

		fullHouseCards.sort(reverse_ranks)

		util0 = DEFAULT_RANKS[fullHouseCards[0].value] * 10e8/2
		util1 = DEFAULT_RANKS[fullHouseCards[1].value] * 10e6/2
		util2 = DEFAULT_RANKS[fullHouseCards[2].value] * 10e4/2
		util3 = DEFAULT_RANKS[fullHouseCards[3].value] * 10e2/2
		util4 = DEFAULT_RANKS[fullHouseCards[4].value] * 10e0/2

		utility = utility + util0 + util1 + util2 + util3 + util4

		return utility, string, fullHouseCards

	# if flush (6)
	if isFlush == 1:
		string = 'Flush with ' + flushSuit
		utility = utility + 6e10

		flushCards.sort(reverse_ranks)

		util0 = DEFAULT_RANKS[flushCards[0].value] * 10e8/2
		util1 = DEFAULT_RANKS[flushCards[1].value] * 10e6/2
		util2 = DEFAULT_RANKS[flushCards[2].value] * 10e4/2
		util3 = DEFAULT_RANKS[flushCards[3].value] * 10e2/2
		util4 = DEFAULT_RANKS[flushCards[4].value] * 10e0/2

		utility = utility + util0 + util1 + util2 + util3 + util4

		return utility, string, flushCards

	# if straight (5)
	if isStraight == 1:
		string = 'Straight of '+straightHighVal
		utility = utility + 5e10
		straightCards.sort(reverse_ranks)

		util0 = DEFAULT_RANKS[straightCards[0].value] * 10e8/2
		util1 = DEFAULT_RANKS[straightCards[1].value] * 10e6/2
		util2 = DEFAULT_RANKS[straightCards[2].value] * 10e4/2
		util3 = DEFAULT_RANKS[straightCards[3].value] * 10e2/2
		util4 = DEFAULT_RANKS[straightCards[4].value] * 10e0/2

		utility = utility + util0 + util1 + util2 + util3 + util4

		return utility, string, straightCards

	# if 3 of a kind (4)
	if is3kind == 1:
		string = 'Three of a Kind of ' + threeKindVal + 's'
		threeKindCards = pydealer.Stack()
		for card in tempFullHouseCards3:
			threeKindCards.add(card)
		for card in currentHand:
			if card not in threeKindCards:
				threeKindCards.add(card)
			if len(threeKindCards) == 5:
				break
		utility = utility + 4e10
		threeKindCards.sort(reverse_ranks)
		util0 = DEFAULT_RANKS[threeKindCards[0].value] * 10e8/2
		util1 = DEFAULT_RANKS[threeKindCards[1].value] * 10e6/2
		util2 = DEFAULT_RANKS[threeKindCards[2].value] * 10e4/2
		util3 = DEFAULT_RANKS[threeKindCards[3].value] * 10e2/2
		util4 = DEFAULT_RANKS[threeKindCards[4].value] * 10e0/2

		utility = utility + util0 + util1 + util2 + util3 + util4

		return utility, string, threeKindCards


	# if 2 pair (3)
	if is2Pair == 1:
		string = 'Two Pair of ' + pairVal + 's and ' + secPairVal + 's'
		for card in pairCards:
			pairCards2.add(card)
		for card in currentHand:
			if card not in pairCards2:
				pairCards2.add(card)
			if len(pairCards2) == 5:
				break
		utility = utility + 3e10

		util0 = DEFAULT_RANKS[pairCards2[0].value] * 10e8/2
		util1 = DEFAULT_RANKS[pairCards2[1].value] * 10e6/2
		util2 = DEFAULT_RANKS[pairCards2[2].value] * 10e4/2
		util3 = DEFAULT_RANKS[pairCards2[3].value] * 10e2/2
		util4 = DEFAULT_RANKS[pairCards2[4].value] * 10e0/2

		utility = utility + util0 + util1 + util2 + util3 + util4

		return utility, string, pairCards2

	#if 1 pair (2)
	if isPair == 1:
		string = 'Pair of ' + pairVal + 's'
		for card in currentHand:
			if card not in pairCards:
				pairCards.add(card)
			if len(pairCards) == 5:
				break

		utility = utility + 2e10

		util0 = DEFAULT_RANKS[pairCards[0].value] * 10e8/2
		util1 = DEFAULT_RANKS[pairCards[1].value] * 10e6/2
		util2 = DEFAULT_RANKS[pairCards[2].value] * 10e4/2
		util3 = DEFAULT_RANKS[pairCards[3].value] * 10e2/2
		util4 = DEFAULT_RANKS[pairCards[4].value] * 10e0/2


		utility = utility + util0 + util1 + util2 + util3 + util4

		return utility, string, pairCards

	#return high card (1)
	
	string = 'High Card ' + revSortedHand[0].value
	highCards = pydealer.Stack()
	for card in currentHand:
		highCards.add(card)
		if len(highCards) == 5:
			break
	utility = utility + 1e10
	util0 = DEFAULT_RANKS[highCards[0].value] * 10e8/2
	util1 = DEFAULT_RANKS[highCards[1].value] * 10e6/2
	util2 = DEFAULT_RANKS[highCards[2].value] * 10e4/2
	util3 = DEFAULT_RANKS[highCards[3].value] * 10e2/2
	util4 = DEFAULT_RANKS[highCards[4].value] * 10e0/2
	utility = utility + util0 + util1 + util2 + util3 + util4
	return utility, string, highCards