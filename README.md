# Poker AI

We will be using the pydealer package found [here](https://pydealer.readthedocs.io/en/latest/index.html).

Refer to [this link](https://docs.python.org/2/tutorial/classes.html) to understand how python classes work.

---

### Project Structure

#### game.py

This script is going to be used for the overall management of the game as it is ongoing. It will ask the player's for their moves when it is their turn and will notify the dealer when it is time for it to deal the next card. In other words, this class will contain the rules of poker and control the flow of the game.

The functions that this sript calls are methods for the pot, player and dealer classes, as well as the AI script for determining the optimal move and the script for determining the winner of a hand (bestHand).

The script is set up to progress chronolgically through the game. At the beginning, it asks for the budget of players and allows the user to add players (either bots or players can be added). By default the first player is a bot (decisions are mde by AI), and the second is an actual user. Each player is a player object (made with the player class) in an array of players, with characteristics described in player.py.

The pot and function classes are used to create new pot and dealer variables. Then, the script enters a for loop that runs the game, and iterates for every round of the poker match.

For every round, the pot is reset and the table cards are reset (using pot.reset() and deal.resetTable(), respectively).

Then, the script iterates through all players, checking at the beginning whether the players have chips. If they are out of chips (play.chips == 0) then the player is marked as out (player.out == 1). With a player being marked out, the player would be skipped in all future iterations of the game. The loop also checks to see whether there are any players lef who are not out. If there is only one player left, then the player is printed to be the winner. By deafult, if the game is not ended in this loop then every player still in the game's hand is reset and dealt a new hand (using the player.resetHand(), player.setHand() and dealer.dealHand() commands).

The game then iterates through the three different card flips for the round. The different card flips (of different numbers of cards depending on whether it is the flop, turn or river) depend on the number of iteration of the loop. The first iteration is the flop, second is turn, and third is river. The dealer class has methods for each card flip type. After flipping the cards, the table and pot are printed, and then each player (via a for loop through all players) is asked for their choice given their own cards and the cards on the table. For a non-AI player, their choice (raise,fold,call,check) is read in through a terminal input and is put throught the player.getChoice() method to implement the changes to the game based on that decision. For the AI players, the choice is gotten from a program that will decide the best choice based on utility of the hand given to the AI player. Afterwards, the pot on the table is adjusted and the highest bet is adjusted (for when other players call).

At the end of each round, the cards of the players who have not folded (and are not out) are compared to decide the winner. The winner gets the pot value added to their chips count!





#### dealer.py

This class is going to manage the table, it will be pinged by game.py when it is time for it to deal the next card. It will also manage the table, so if game.py or player.py need to access the cards on the table, they will get this from dealer.py. game.py will be in charge of making sure the functions are only called when they're supposed to be called.

The functions it needs are as follows:

**dealFlop()** - discards one card and deals three onto the table

**dealTurn()** - discards one card and deals one onto the table

**dealRiver()** - discards one card and deals one onto the table

**resetTable()** - resets the table for the next hand

**getTable()** - returns the cards currently on the table


#### player.py

This class will encapsulate one player in the game. This means it will have the option to call, check, raise, or fold. It will contain the player's number of chips and their hand.

**getChoice(bot)** - this function will return a string of one of the following: "check", "call", "raise", or "fold". If the player chooses to raise, they will also be expected to provide an amount. If the getChoice method is reading the decision of a bot (bot==1), then the AI logic is run the get the choice of the AI program.


**setHand(hand)** - give the player their hand for the round

**resetHand()** - resets the player's hand so they can receive new cards in the next round.

**changeChips(amount)** - if the player bets money or wins a hand, this function will be used to modify the number of chips they have.

**processChoice(choice, highestBet)** - decides what to do for each choice made on each hand. If the person raises, the function will ask how much the person will raise, and adjusts the pot and the persons budget. If the person's budget becomes negative, the changes to the pot and persons budget are reverted and the function calls itself again to ask the person to change their bet amount. If the person calls, the person bets the amount of highestBet, and the pot and chips of the persona are adjuested accordingly. If the person cannot match the highest bet, then the person is asked to make another decision and processChoice() is called again with that choice. If the person folds then the players fold attribute is changed to 1. The function returns the amount to be added to the pot.

**calcRaiseAmount(curWinProb, minimumBetAmount, playersLeft, bigBlindVal)** -determines the amount to bet based on its probability of winning, how many players are left, the the size of the blind.

**shouldCall(curWinProb, callAmount, potSize, bigBlindVal)** - decides if it should call based off of probability of winning and payoff of staying in pot.

**shouldRaise(curWinProb, callAmount, potSize, bigBlindVal)** - decides if it should raise someone's bet based off of probability of winning and payoff of staying in pot.

**shouldBet(curWinProb, potSize, playersLeft)** - determines if it should bet in a check/bet situation.

#### pot.py

This class will simply contain the current pot and will award the amount in the pot to the winner of the hand.

**getSize()** - returns the size of the pot

**add(amount)** - adds a player's bet to the pot

**reset()** - resets the pot for the next hand

#### bestHand.py

This class contains a function that returns the player's highest set, top 5 cards, and utility of their hand

**findBestHand(hand, table)** - returns player's utility, a string that contains what set (if any) the player has, and a stack containing their top 5 cards

#### sim.py

This class contains a method for running the Monte Carlo method to predict the likelihood you win a round of poker. Right now, it's structured as a class so you create an instance of it by saying

```
import sim
mySim = sim.Sim(myHand, table, numOpponents)
```

where myHand is the AI's hand, table is the cards on the table, and numOpponents is the number of opponents who haven't folded.

After you have created the instance, all you have to do is run the `run` method and pass it the number of iterations.

```
mySim.run(10000)
```

and it will return the decimal probability you have for winning that hand.
