import pydealer

class Pot:

    def __init__(self):
        self.size = 0

    def getSize(self):
        return self.size

    def add(self, amount):
        self.size += amount

    def reset(self):
        self.size = 0
