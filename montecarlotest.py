import pydealer
import sim
import time
import numpy as np





for k in range(1, 6):
    prop = list()

    for i in range(100):
        deal = pydealer.Deck()
        deal.shuffle()

        me = deal.deal(2)
        table = deal.deal(3)

        mySim = sim.Sim(me, table, k)

        start = time.time()
        chance = mySim.run(100)
        end = time.time()
        prop.append(chance)



    print('With ' + str(k) + ' opponents, I won ' + str(np.average(prop) * 100) + ' percent of the time')

# deal = pydealer.Deck()
# deal.shuffle()
#
# me = deal.deal(2)
# table = deal.deal(3)
#
# mySim = sim.Sim(me, table, 1)
#
# start = time.time()
# chance = mySim.run(5)
